import countries from '../data/countries.json'
import leagues from '../data/leagues.json'
import games from '../data/games.json'

export const root = {
    connect: () => {
      return 'Connected to the versus api!';
    },
    countries: () => {
        return countries;
    },
    leagues: () => {
        return leagues;
    },
    games: () => {
        return games;
    }
  };
