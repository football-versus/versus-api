var { buildSchema } = require('graphql');

import Game from './Game.gql'
import Country from './Country.gql'
import League from './League.gql'
import Player from './Player.gql'
import Statistic from './Statistic.gql'
import Substitution from './Substitution.gql'
import Card from './Card.gql'
import GoalScorer from './GoalScorer.gql'
import Team from './Team.gql'

const Query = `
    type Query {
        connect: String,
        countries: [Country!]!,
        leagues: [League!]!,
        games: [Game!]!
    }
`

const Schema = Query.concat(
    Country,
    Game,
    League,
    Player,
    Statistic,
    Substitution,
    Card,
    Team,
    GoalScorer
);

export const schema = buildSchema( Schema );