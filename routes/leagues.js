import leagues from '../data/leagues';
var express = require('express');
var router = express.Router();

router.get('/:countryname', function(req, res, next) {

  const _leagues = leagues.filter(league => 
    league.country_name.toLowerCase() === req.params.countryname.toLowerCase());

  res.json(_leagues)
});

router.get('/', function(req, res, next) {
  res.json(leagues);
});

module.exports = router;
