import countries from '../data/countries';
var express = require('express');
var router = express.Router();

router.get('/name', (req, res) => {
  const country = countries.filter(country => 
    country.country_name.toLowerCase() === req.params.name.toLowerCase())[0];
  
  res.json(country);
})

router.get('/', (req, res, next) => {
  res.json(countries);
});

module.exports = router;
