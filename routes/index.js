import {root} from '../graphql/root'
import {schema} from '../graphql/schema'
const express = require('express')
const router = express.Router()
var graphqlHTTP = require('express-graphql')

router.use(
  '/graphql',  // url
  graphqlHTTP({ // middleware
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

module.exports = router;
