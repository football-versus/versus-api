# [Versus - API proxy]
---
[![Total alerts](https://img.shields.io/lgtm/alerts/b/football-versus/versus-api.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-api/alerts/)
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/b/football-versus/versus-api.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-api/context:javascript)

## Installation

From the project root run 

`npm i`
`npm start`

## Useage
Pull up the sandbox with
`localhost:3000/graphql`

Example query
`
{
  games {
    match_hometeam_name
    match_awayteam_name
  }
}
`

## Deployment
Travis deployment coming soon...

Live link: `http://ec2-54-77-155-243.eu-west-1.compute.amazonaws.com:3000/graphql`

Example useage `http://ec2-54-77-155-243.eu-west-1.compute.amazonaws.com:3000/graphql?query=%7Bgames%7Bmatch_hometeam_name,match_hometeam_score,match_awayteam_name,match_awayteam_score%7D%7D`
